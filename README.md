# Requirements
* **npm >=5.0.0** If you run Windows and installed the *LTS* version of Node.js, you probably need to install the 
*current* version. You can download and run the .msi version to upgrade from the old version.
* **webpack >=3.0.0** Install globally with `npm install webpack -g` via your terminal.

@link test

# Getting Started
So, you have just forked the Boilerplate project and want to get it running? Follow these steps:

1. Make sure you have the required libraries mentioned above.
2. Run `npm install` in the terminal to let the package manager download whatever you need.
	- If a **package-lock.json** is created, commit it to the repository.
3. Run `npm run watch` to create a development build at "/dist".
	- You should be able to run the project in your browser now.
4. In the **package.json** change the `"name"` field to something a sensible project name.
	- Go with a pattern like "genre-franchisename". For example, "endlessrunner-starwars".
	- Don't forget to commit.
5. In the **package.json** reset the `"version"` to "0.0.0".
	- Refer to the **Versions** section to learn how to bump the project version.
	- Don't forget to commit.
6. Refer to the **Bitbucket Pipelines** section to set up pipelines. 
7. Enable ESLint in WebStorm.
	1. Go to File > Settings.
	2. In the **Settings** dialogue, go to: Languages & Frameworks > JavaScript > Code Quality Tools > ESLint.
	3. In the ESLint dialogue turn on the "Enable" checkbox.
	4. If the ESLint didn't detect the **ESLint package**, point it to "\node_modules\eslint\" in your poject's root folder.
	5. Refer to the **ESLint** section to learn what it is and why it is important for us.

# Bitbucket Pipelines
* Change project name and version in package.json if you haven't already.
* Visit project on Bitbucket and activate pipelines.
* Add pipelines SSH key (ask Daniel) in Bitbucket settings.
* Re-run build manually or automatically on next commit.
* Versioned build are available under Bitbucket downloads and on (https://test.flying-sheep.com/bb/)

# Versions
* *Regularly* bump your version as it makes sense! 
* `npm version` is your friend. @see: https://docs.npmjs.com/cli/version
* In your *clean* repo, run `npm version [major | minor | patch]` to increment `major.minor.patch` and automatically create a commit and tag.

# Phaser Guidelines
## Switching to a Phaser Only Project
By default the Boilerplate has both Phaser and Three.js libraries active. If you just need to use the Phaser library follow these steps:
* Open "src/js/index.js".
* In the SheepCore.Game object creation, set the `"three"` parameter to `"false"`.

Don't forget to remove all the Three.js example code in the example state. Moreover, remove the example 3D models and textures. 

## Changing Renderer Type
By default the Phaser Renderer is set to `Phaser.CANVAS`. This works well for 3D games since rendering two WebGL canvases can be expensive. 
If you want to change the renderer, you can set it as a parameter while creating the `SheepCore.Game` object in **index.js**.
Following is an example of changing your Phaser renderer to WebGL: 
```javascript
const game = new SheepCore.Game({
	width: 720,
	height: 1280,
	three: false,
	enableDebug: false,
	phaserParameters: {
		renderer: Phaser.WEBGL
	}
});
```

# Debug Info
Debug information is shown on the top left of the screen. It is enabled by default.

## Turning off Debug Dialogue
By default the Debug information is active. It has to be turned off before making a build for a client. Follow these steps:
* Open "src/js/index.js".
* In the SheepCore.Game object creation, set the `enableDebug:` parameter to `false`.

## Adding Phaser Plugins
All Phaser dependencies are already included in webpack. In case you need to install a Phaser based plugin (Particle Storm, Virtual Joystick), you will need to add those manually.
Following are the repositories for the available plugins. Head over to the **Adding External Libraries (ES6)** to learn how to include them.
- [Phaser Particle Storm](https://bitbucket.org/sheeple/phaser-particlestorm/src/master/) - Phaser's particle effects library.
- [Phaser Virtual Joystick](https://bitbucket.org/sheeple/phaser-virtual-joystick-plugin/src/master/) - Phaser's onscreen joystick or D-Pad library.

Note that once included, you do not need to import plugins using the `import` statement. They will be implicitly available to use under the `Phaser` object.
For example `Phaser.VirtualJoystick` and `Phaser.ParticleStorm`.

#### Pro Tip: Debug-only Code in your Game 
If you want to have debug code that should only be active in internal builds e.g. a `console.log` or a cheat etc. always write that code under the `this.game.config.enableDebug` condition. For example:
```javascript
if (this.game.config.enableDebug) {
   console.log("menu created");
}
```

#### Pro Tip: Add text to the Debug Info
If you want to show a custom information on the debug information on the top left, you can append it like so:
```javascript
this.game.debug.extraText = "debug text";
```

# SheepUI
### Introduction
We use a in-house developed UI editor by the name of SheepUI. It allows the designers to create a UI using the Phaser library without writing any code.
They then export a JSON file with all the information the SheepUI API needs to translate their work and add in-game assets and menus.

You should be able to see an example of the SheepUI in action in the **Menu.js** and **ThreeExample.js** states.

# FAQs
#### I move a SheepUI sprite/ button but it snaps back to its original location.
Lets say your element is called **object**. Use the `object.shui.position` to move your element.

#### I want to render the SheepUI menus on the very top.
SheepUI is organized into groups just as any Phaser elements. You can do that by simply `this.game.world.bringToTop(this.layout);`


# ESLint
### What is it?
ESLint is an open source JavaScript linting utility. It allows us to enforce a coding style standard. You can learn more about it [here](https://eslint.org/).

### Why is it important to us?
* We use it to enforce our coding guidelines. 
* Your code should have no ESLint errors before making a production build or the build will fail.
	- The Bitbucket pipeline build will also fail on ESLint errors. 
* Ideally, your code should have minimal ESLint warnings. 

# Naming Conventions
### Variable Names
* Variable names should always be in camel case. 
	- Good example: `myOwnVar`
	- Bad examples `MyVar`, `myvar`, `Myvar`
* Following ES6 standard, we use `let` to declare a variable instead of `var`. 
	- Good example: `let myVar`
	- Bad example: `var myVar`

### Constant Names
* Constants are declared using the `const` key word.
	- Good example: `const myConst`, `const MY_CONST`

### Class Names
*  Classes are named using camel case with the first character capitalized. 
	- Good example: `class MyClass`

# Adding External Libraries
## Adding an ES5 Library
1. Open **package.json**.
2. Add the npm package name in the `"dependencies"` section.
3. Run `npm install` to download the package.
4. Import the package in your classes using the `"import"` keyword.

## Adding an ES6 Library
### 1. Add Library to Package Manager
1. Open **"package.json"**.
2. Add the npm package name in the `"dependencies"` section.
3. Run `npm install` to download the package.

### 2. Add the Library Reference to Webpack Common
Now that we have the package in our repository, we need to include it using webpack. Let's use "sheepmation" as an example.
Open **webpack.common.js** and in the `module.exports.module.rules` array, add another rule object for the new ES6 library.
	
- The `test` member should have the regex with the library name.
- The `use` member should have the class name.

Our example will look as follows:
```
{
	test: /sheepmation(?:\.min)?\.js/,
	use: ["expose-loader?SheepMation", "exports-loader?SheepMation"]
}
```

### 3. Add the Library Path to Webpack Development
Open **webpack.development.js** and add a constant with the path of the library like so:
```javascript
const sheepMation = path.join(__dirname, "node_modules/sheepmation/lib/SheepMation.js");
```
Next, add the alias to the `resolve.alias` object like so: `"sheepmation": sheepMation`

Finally, append the alias in the `module.rules.exclude` object like so: `|sheepmation`.

### 4. Add the Libaray Path to Webpack Production 
Open the **webpack.production.js** file and repeat the same steps as above.

When you're done, run `npm run watch` again so that a new build is created with the changes. 

# Using the Toggo API
Since Toggo is our most frequented client, it is worth having a section just for their API. The Toggo API is used to implement the highscore system and event tracking.

## Integrating the Toggo API
- Follow the "Adding an ES6 Library" section and add the Toggo API found at this [Bitbucket link](https://bitbucket.org/sheeple/toggo_api_es6/src/master/).
- Open the **src/js/state/Boot.js** script.
- Create a constant with the Toggo key. You can use the name you set in **package.json**. For example **runner-turtles**.
	- The Toggo back-end uses this name as the Primary Key to store the scores and tracking data so make sure it is unique and never reused.
	- It is a worthwhile to store this globally since it will be used in multiple scripts. You can add it as a member of `this.game` object like so: `this.game.toggoKey = "runner-turtles";`.
- Import the Toggo API like the following: 
```javascript
import {FSToggo} from "fstoggoapi";
```
- Create a new object using the key you created in the last step like so: 
```javascript
this.game.toggoApi = new FSToggo.API(this.game.toggoKey);
```
- Note that we add its reference to `this.game` so that we can use this in all needed scripts later on.
	
## Tracking with Toggo API
We only track screen change events by using `toggoApi.track("event");`
Please refer to [this guide](https://docs.google.com/document/d/1vevv7UeZ9-Zfq7WaRljfw_HoS8ZLXxWg8JSrUmQVenY/edit) to understand what to track. 
Formally, to continue our above example, this is how the main menu start event will be tracked:
```javascript
this.game.toggoApi.track("menue");
```

## Using the Toggo Highscore API
We can use the Toggo highscore API to submit a player highscore on the Game Over or Name Entry screen or retrieve a list of highscores to display then on the Highscore screen. 

### Submitting Player Score
You can submit a score by using the following syntax:
```javascript
this.game.toggoApi.pushScore("playerName", "playerScore");
```

### Retrieving Highscore List
You can get player score by using the following function call. This will get you the name and scores of the top 100 players.
```
this.game.toggoApi.pullScores(99);
```

Once you have request the score list, you will receive a signal when the scores have been received. 
You can add a listener and display the score list once you get the data. Following is an example of how this can be accomplished:
```javascript
this.game.toggoApi.addOnceListener(this.game.toggoApi.event.LOADED, (data) => {
	// Only 10 since we only display 10 ten players
	for (let i = 0; i < 10; i++) {
		this.textScoreList[i].setText(data[i].score);
		this.textNameList[i].setText(data[i].nickName);
	}
	
	// Get the player score and position
	for (let i = 0; i < data.length; i++) {
		if (data[i].nickName  === playerName) {
			this.textPlayerName.setText(playerName);
			this.textPlayerRank.setText(i + 1);
			this.textPlayerScore.setText(data[i].score);
			break;
		}
	}
});
```
 
If you are going to use the highscore API for the first time please refer to an existing code or an a programmer to learn about the best practices and quirks.
